<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Main extends CI_Controller {
 
	function __construct()
	{
	        parent::__construct();
	 
	$this->load->model('employees_model');
	$this->load->helper('url'); 
	}
	 
	public function index()
	{
	  $output = $this->employees_model->viewdb();
	  $this->data['employeelist'] = $output;
      $this->load->view('employees', $this->data);
	}

	public function update() {
	    $employee = array(
	    	'data' => array(
    			'lastName' => $_POST['lname'] ,
	   			'firstName' => $_POST['fname'] ,
	   			'jobTitle' => $_POST['job']
	    	),
	    	'id' => $_POST['num'],
        );
        $this->employees_model->update($employee);
	}

	public function delete() {


		$this->employees_model->delete($_POST['num']);
	}

	public function add() {
		$data = array(
		   'lastName' => $_POST['lname'] ,
   			'firstName' => $_POST['fname'] ,
   			'jobTitle' => $_POST['job']
		);

        $employee_id = $this->employees_model->add($data);
		echo $employee_id;
		// Produces: INSERT INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
 
/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
 