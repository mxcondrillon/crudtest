<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employees_model extends CI_Model {

    
	public function __construct()
	{
		parent::__construct();

	}

    public function viewdb()
    {
        $query = $this->db->select('*')->from('crud')->get();
        return $query->result();
    }

    public function update($employee) {

		$this->db->where('employeeNumber', $employee["id"]);
		$this->db->update('crud', $employee['data']); 

		// Produces:
		// UPDATE crud
		// SET title = '{$title}', name = '{$name}', date = '{$date}'
		// WHERE id = $id
	}

	public function delete($id) {

		$this->db->delete('crud', array('employeeNumber' => $id)); 
		// Produces:
		// DELETE FROM mytable
		// WHERE id = $id
	}

	public function add($data) {

		$this->db->insert('crud', $data); 
		return $this->db->insert_id();
		// Produces: INSERT INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>